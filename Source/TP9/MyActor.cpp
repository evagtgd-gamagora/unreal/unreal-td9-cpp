// Fill out your copyright notice in the Description page of Project Settings.

#include "MyActor.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	maRacine = CreateDefaultSubobject<USceneComponent>("maRacine");
	monMaillage = CreateDefaultSubobject<UStaticMeshComponent>("monMaillage");
	joueur = CreateDefaultSubobject<AActor>("joueur");
	monMaillage->AttachTo(maRacine);
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();	
	UE_LOG(LogActor, Warning, TEXT("Hello World"));
	UE_LOG(LogActor, Warning, TEXT("Actor name %s"), *this->GetName());

	int i = 99;
	UE_LOG(LogActor, Error, TEXT("Test affichage erreur std avec un entier %d"), i);

	FString nom = FString::Printf(TEXT("Actor name %s"), *this->GetName());
	UE_LOG(LogActor, Warning, TEXT("%s"), *nom);
	
	positionRelative = monMaillage->GetRelativeTransform().GetLocation();
	distanceJoueur = GetDistanceTo(joueur);
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	this->SetActorLocation(this->GetActorLocation() + 10 * FVector(0, 0, 1) * (distanceJoueur - GetDistanceTo(joueur)));
	distanceJoueur = GetDistanceTo(joueur);

	
}

